package com.example.primera_prueba;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.LENGTH_SHORT;

public class Main2Activity extends AppCompatActivity {
    private EditText txt_precio,txt_nombre,txt_tipo;
    private Button btn_add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        txt_precio=(EditText)findViewById(R.id.txt_precio);
        txt_nombre=(EditText)findViewById(R.id.txt_nombre);
        txt_tipo=(EditText)findViewById(R.id.txt_tipo);
        btn_add=(Button)findViewById(R.id.btn_add);
    }

    public void agregar(View view) {
        // Access a Cloud Firestore instance from your Activity

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //FirebaseInstanceId dbd =FirebaseInstanceId.getInstance();

        String nombre=this.txt_nombre.getText().toString();
        String tipo=this.txt_tipo.getText().toString();
        String precio=this.txt_precio.getText().toString();

        Map<String,Object> user=new HashMap<>();

        if(!(TextUtils.isEmpty(tipo)&&TextUtils.isEmpty(precio)&&TextUtils.isEmpty(nombre)))
        {

            // Toast.makeText(this, "Ingreso", Toast.LENGTH_SHORT).show();
            int valor=Integer.parseInt(txt_precio.getText().toString());
            user.put("Nombre",nombre);
            user.put("Valor",valor);
            user.put("Tipo",tipo);

            db.collection("productos2")
                    .add(user)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            //Log.d("veg", "DocumentSnapshot written with ID: " + documentReference.getId());
                            Toast.makeText(Main2Activity.this, "Ingreso exitoso", LENGTH_SHORT).show();
                            txt_nombre.setText("");
                            txt_precio.setText("");
                            txt_tipo.setText("");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //Log.w("veg", "Error adding document", e);
                            Toast.makeText(Main2Activity.this, "Ingreso erroneo",Toast.LENGTH_SHORT).show();
                        }
                    });

        }else
        {
            Toast.makeText(this, "Ingreso de datos incorrecto", LENGTH_SHORT).show();
        }
    }

    public void regresar(View view) {
        finish();
    }

    }

