package com.example.primera_prueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtUser,txtPass;
    private Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUser= txtUser.findViewById(R.id.txtUser);
        txtPass= txtPass.findViewById(R.id.txtPass);
        btnLogin=btnLogin.findViewById(R.id.btnLogin);
    }

    public void Login(View view) {
        String user=this.txtUser.getText().toString();
        String pass=this.txtPass.getText().toString();
        //Toast.makeText(this, "ingreso", Toast.LENGTH_SHORT).show();
        if (!(TextUtils.isEmpty(user)&&TextUtils.isEmpty(pass)))
        {
            if(TextUtils.isEmpty(user)||TextUtils.isEmpty(pass))
            {
                Toast.makeText(getApplicationContext(),"Inicio de sesion incorrecto",Toast.LENGTH_SHORT).show();
            }else
            {
                //Toast.makeText(this, "ingreso", Toast.LENGTH_SHORT).show();
                if(user.equals("admin")&&pass.equals("admin"))
                {
                    Intent intent=new Intent(MainActivity.this,Main2Activity.class);
                    startActivity(intent);
                    txtUser.setText("");
                    txtPass.setText("");
                }else
                {
                    Intent intent=new Intent(MainActivity.this,Main3Activity.class);
                    startActivity(intent);
                }
            }

        }else
        {
            Toast.makeText(this,"Inicio de sesion incorrecto",Toast.LENGTH_SHORT).show();
        }

    }

}
